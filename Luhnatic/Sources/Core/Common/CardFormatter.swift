//
//  CardFormatter.swift
//  Luhnatic
//
//  Created by Ilia Gutu on 2/18/20.
//  Copyright © 2020 Ilia Gutu. All rights reserved.
//

import Foundation

public struct CardFormatter {
    
    func cardType(_ cardNumber: String) throws -> CardType {
        guard let cardType = CardType.all.first(where: { cardType in
            let predicate = NSPredicate(format: "SELF MATCHES %@", cardType.regex)
            return predicate.evaluate(with: cardNumber)
        }) else { throw CardValidatorError.nonSupportedCard }
        
        return cardType
    }

    func formattedCardNumber(_ cardNumber: String) -> String {
        let numbersOnlyEquivalent = cardNumber.replacingOccurrences(of: "[^0-9]", with: "",
                                                                    options: .regularExpression,
                                                                    range: nil)
        return numbersOnlyEquivalent.trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
