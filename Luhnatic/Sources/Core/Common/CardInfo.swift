//
//  CardInfo.swift
//  Luhnatic
//
//  Created by Ilia Gutu on 2/18/20.
//  Copyright © 2020 Ilia Gutu. All rights reserved.
//

import Foundation

// MARK: - CardInfo
public struct CardInfo {
    let number: Number
    let scheme, type, brand: String
    let prepaid: Bool
    let country: Country
    let bank: Bank
}

extension CardInfo: Codable { }

// MARK: - Bank
public struct Bank: Codable {
    let name, url, phone, city: String
}

// MARK: - Country
public struct Country: Codable {
    let numeric, alpha2, name, emoji: String
    let currency: String
    let latitude, longitude: Int
}

// MARK: - Number
public struct Number: Codable {
    let length: Int
    let luhn: Bool
}
