//
//  CardType.swift
//  Luhnatic
//
//  Created by Ilia Gutu on 2/18/20.
//  Copyright © 2020 Ilia Gutu. All rights reserved.
//

import Foundation

public enum CardType: Int, CaseIterable {
    case amex = 0
    case visa
    case mastercard
    
    static let all:[CardType] = allCases
}

extension CardType {
    
    var regex: String {
        switch self {
        case .amex:
            return "^3[47][0-9]{5,}$"
        case .mastercard:
            return "^5[1-5][0-9]{5,}|222[1-9][0-9]{3,}|22[3-9][0-9]{4,}|2[3-6][0-9]{5,}|27[01][0-9]{4,}|2720[0-9]{3,}$"
        case .visa:
            return "^4[0-9]{6,}$"
        }
    }
}

public extension CardType {
    func stringValue() -> String {
        switch self {
        case .amex:
            return "American Express"
        case .visa:
            return "Visa"
        case .mastercard:
            return "Mastercard"
        }
    }
    
    init?(string: String) {
        switch string.lowercased() {
        case "american express":
            self.init(rawValue: 0)
        case "visa":
            self.init(rawValue: 1)
        case "mastercard":
            self.init(rawValue: 2)
        default:
            return nil
        }
    }
}
