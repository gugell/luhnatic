//
//  URLSessionType.swift
//  Luhnatic
//
//  Created by Ilia Gutu on 2/19/20.
//  Copyright © 2020 Ilia Gutu. All rights reserved.
//

import Foundation

protocol URLSessionType {
    func dataTask(with request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void) -> Task
}

extension URLSession: URLSessionType {
    func dataTask(with request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void) -> Task {
        return dataTask(with: request, completionHandler: completion)
    }
}

protocol Task {
    func resume()
    func cancel()
}

extension URLSessionTask: Task {}
