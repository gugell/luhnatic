//
//  BinlistCardDataProvider.swift
//  Luhnatic
//
//  Created by Ilia Gutu on 2/18/20.
//  Copyright © 2020 Ilia Gutu. All rights reserved.
//

import Foundation

public final class BinlistCardDataProvider: CardDataProviderType {
    private let urlSession: URLSessionType
    private let baseURL = URL(string: "https://lookup.binlist.net")!
    private var task: Task?
    private let cardFormatter = CardFormatter()
    
    init(urlSession: URLSessionType) {
        self.urlSession =  urlSession
    }
    
    public func getInfo(_ cardNumber: String, completion: @escaping (Result<CardInfo, CardDataProviderError>) -> Void) {
        task?.cancel()
        let formattedCardNumber = cardFormatter.formattedCardNumber(cardNumber)
        let request = URLRequest(url: baseURL.appendingPathComponent(formattedCardNumber))
        task = urlSession.dataTask(with: request) { [weak self] data, response, error in
            self?.handleDataResponse(data: data, response: response, error: error, completion: completion)
        }
        task?.resume()
    }
    
    private func handleDataResponse(data: Data?, response: URLResponse?, error: Error?, completion: (Result<CardInfo, CardDataProviderError>) -> Void) {
        guard error == nil else { return completion(.failure(.failure(error))) }
        guard let response = response else { return completion(.failure(.badResponse)) }
        
        if let httpResponse = response as? HTTPURLResponse {
            
            do {
                try httpResponse.checkSuccessfulStatusCodes()
                guard let data = data else { return completion(.failure(.badResponse)) }
                let cardInfo = try JSONDecoder().decode(CardInfo.self, from: data)
                completion(.success(cardInfo))
            } catch {
                switch httpResponse.statusCode {
                case 429:
                    completion(.failure(.speedLimitReached))
                default:
                    completion(.failure(CardDataProviderError(error: error)))
                }
            }
        } else {
            completion(.failure(.nonHTTPResponse))
        }
    }
}
