//
//  CardDataProviderError.swift
//  Luhnatic
//
//  Created by Ilia Gutu on 2/19/20.
//  Copyright © 2020 Ilia Gutu. All rights reserved.
//

import Foundation

public enum CardDataProviderError: Error, Equatable, LocalizedError {
    case invalidCard
    case nonHTTPResponse
    case speedLimitReached
    case badResponse
    case badStatusCode(Int)
    case failure(Error?)
    
    var localizedDescription: String {
        switch self {
        case .nonHTTPResponse:
            return "Non HTTP response"
        case .invalidCard:
            return "Invalid card provided"
        case .speedLimitReached:
            return "Speed limit reached.Please upgrade or wait for 10 min."
        case .badResponse:
            return "Bad response"
        case .badStatusCode(let statusCode):
            return "Status code \(statusCode) didn't fall within the given range."
        case .failure(let error):
            return error?.localizedDescription ?? "Something went wrong."
        }
    }
    
    init(error: Error?) {
        if let error = error as? CardDataProviderError {
            self = error
        } else {
            self = .failure(error)
        }
    }
    
    public static func == (lhs: CardDataProviderError, rhs: CardDataProviderError) -> Bool {
        switch (lhs, rhs) {
        case (.badResponse, .badResponse):
            return true
        case (.invalidCard, .invalidCard):
                return true
        case (.speedLimitReached, .speedLimitReached):
                   return true
        case (.nonHTTPResponse, .nonHTTPResponse):
            return true
        case (.badStatusCode(let lhsCode), .badStatusCode(let rhsCode)):
            return lhsCode == rhsCode
        case (.failure(let lhsError), .failure(let rhsError)):
            return lhsError?.localizedDescription == rhsError?.localizedDescription
        default:
            return false
        }
    }
    
    public var errorDescription: String? { return localizedDescription }
}
