//
//  CardDataProviderType.swift
//  Luhnatic
//
//  Created by Ilia Gutu on 2/18/20.
//  Copyright © 2020 Ilia Gutu. All rights reserved.
//

import Foundation

public protocol CardDataProviderType {
    func getInfo(_ cardNumber: String, completion: @escaping (Result<CardInfo, CardDataProviderError>) -> Void)
}
