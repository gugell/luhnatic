//
//  CardValidatorError.swift
//  Luhnatic
//
//  Created by Ilia Gutu on 2/20/20.
//  Copyright © 2020 Ilia Gutu. All rights reserved.
//
import Foundation

public enum CardValidatorError: Error, LocalizedError {
    case invalidCardNumber
    case invalidCard
    case nonSupportedCard
    
    var localizedDescription: String {
        switch self {
        case .invalidCardNumber:
             return "Card number should be in range of 12..19 digits"
        case .nonSupportedCard:
            return "Non supported card provided"
        case .invalidCard:
            return "Invalid card provided"
        }
    }

    public var errorDescription: String? { return localizedDescription }
}
