//
//  CardValidatorType.swift
//  Luhnatic
//
//  Created by Ilia Gutu on 2/18/20.
//  Copyright © 2020 Ilia Gutu. All rights reserved.
//

import Foundation

public protocol CardValidatorType {
    func validate(_ cardNumber: String) throws
}
