//
//  LuhnCardValidator.swift
//  Luhnatic
//
//  Created by Ilia Gutu on 2/18/20.
//  Copyright © 2020 Ilia Gutu. All rights reserved.
//

import Foundation

public final class LuhnCardValidator: CardValidatorType {
    
    static let instance = LuhnCardValidator()
    private let cardFormatter = CardFormatter()

    public func validate(_ cardNumber: String) throws {
        let formattedCardNumber = cardFormatter.formattedCardNumber(cardNumber)
        try preconditionsCheck(formattedCardNumber)
        guard luhnCheck(formattedCardNumber) else { throw CardValidatorError.invalidCard }
    }

    private func luhnCheck(_ number: String) -> Bool {
        var sum = 0
        let digitStrings = number.reversed().map { String($0) }
        for (idx, element) in digitStrings.enumerated() {
            let isOdd = idx % 2 == 1
            guard let digit = Int(element) else { return false }
            switch (isOdd, digit) {
            case (true, 9): sum += 9
            case (true, 0...8): sum += (digit * 2) % 9
            default: sum += digit
            }
        }
        return sum % 10 == 0
    }

    private func preconditionsCheck(_ number: String) throws {
        guard number.count >= 12 && number.count <= 19 else {
            throw CardValidatorError.invalidCardNumber
        }
        _ = try cardFormatter.cardType(number)
    }
}
