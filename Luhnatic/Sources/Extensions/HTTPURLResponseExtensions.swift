//
//  HTTPURLResponseExtensions.swift
//  Luhnatic
//
//  Created by Ilia Gutu on 2/19/20.
//  Copyright © 2020 Ilia Gutu. All rights reserved.
//

import Foundation

extension HTTPURLResponse {
    func filter<R: RangeExpression>(statusCodes: R) throws where R.Bound == Int {
        guard statusCodes.contains(statusCode) else {
            throw CardDataProviderError.badStatusCode(statusCode)
        }
    }
    
    func checkSuccessfulStatusCodes() throws {
        return try filter(statusCodes: 200...299)
    }
}
