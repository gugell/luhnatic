//
//  BinlistCardDataProviderTests.swift
//  LuhnaticTests
//
//  Created by Ilia Gutu on 2/20/20.
//  Copyright © 2020 Ilia Gutu. All rights reserved.
//
import XCTest
@testable import Luhnatic

final class BinlistCardDataProviderTests: XCTestCase {

    private var mockUrlSession: MockURLSession!
    private var binlistProvider: BinlistCardDataProvider!
    
    override func setUp() {
        super.setUp()
        mockUrlSession = MockURLSession()
        binlistProvider = BinlistCardDataProvider(urlSession: mockUrlSession)
    }

    override func tearDown() {
        super.tearDown()
    }
    
    func test_whenApiReturnsValidData_shouldReturnValue() {
        let url =  URL(string: "https://some/url")!
        let statusCode = 200
        
        let json =
        """
{
          "number": {
            "length": 16,
            "luhn": true
          },
          "scheme": "visa",
          "type": "debit",
          "brand": "Visa/Dankort",
          "prepaid": false,
          "country": {
            "numeric": "208",
            "alpha2": "DK",
            "name": "Denmark",
            "emoji": "🇩🇰",
            "currency": "DKK",
            "latitude": 56,
            "longitude": 10
          },
          "bank": {
            "name": "Jyske Bank",
            "url": "www.jyskebank.dk",
            "phone": "+4589893300",
            "city": "Hjørring"
          }
        }
"""
        
        let data = json.data(using: .utf8)!
        
        var result: Result<CardInfo,CardDataProviderError>?
        
        mockUrlSession.dataTaskStub = (
            data,
            HTTPURLResponse(url: url, statusCode: statusCode, httpVersion: nil, headerFields: nil),
            nil
        )

        binlistProvider.getInfo("545567676666") { response in
            result = response
        }
        guard case .success(let response) = result else { return XCTFail() }
        XCTAssert(response.number.luhn == true)
    }
    
        func test_whenApiReturnsError_shouldFail() {
            let url =  URL(string: "https://some/url")!
            let statusCode = 400
            
            var result: Result<CardInfo,CardDataProviderError>!
            let error = CardDataProviderError.badStatusCode(statusCode)
            mockUrlSession.dataTaskStub = (
                nil,
                HTTPURLResponse(url: url, statusCode: statusCode, httpVersion: nil, headerFields: nil),
                error
            )

            binlistProvider.getInfo("545567676666") { response in
                result = response
            }
            guard case .failure(let failure) = result else { return XCTFail() }
            let reason = failure.errorDescription
            XCTAssert(reason == "Status code 400 didn't fall within the given range.")
        }
}

