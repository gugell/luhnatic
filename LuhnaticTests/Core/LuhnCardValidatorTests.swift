//
//  LuhnCardValidatorTests.swift
//  LuhnaticTests
//
//  Created by Ilia Gutu on 2/20/20.
//  Copyright © 2020 Ilia Gutu. All rights reserved.
//
import XCTest
@testable import Luhnatic

final class LuhnCardValidatorTests: XCTestCase {

    private var cardValidator: LuhnCardValidator!
    
    override func setUp() {
        super.setUp()
        cardValidator = LuhnCardValidator.instance
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_whenCardIsValid_shouldPassLuhnCheck() {
        let cardnumber = "4779 1868 3277 7666"
        do {
            try cardValidator.validate(cardnumber)
        } catch {
             XCTFail()
        }
    }
    
    func test_whenCardIsValid_shouldFailLuhnCheck() {
        let cardnumber = "4779 1868 3277 7"
        do {
            try cardValidator.validate(cardnumber)
            XCTFail("Card check should not pass check because it is incomplete")
        } catch { }
    }
    
    func test_whenCardIsValidAndUnFormatted_shouldPassLuhnCheck() {
          let cardnumber = "4779186832777666"
          do {
              try cardValidator.validate(cardnumber)

          } catch {
            XCTFail("Card check should pass")
        }
    }
}

