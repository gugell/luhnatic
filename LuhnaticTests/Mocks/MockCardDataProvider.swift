//
//  MockCardDataProvider.swift
//  LuhnaticTests
//
//  Created by Ilia Gutu on 2/20/20.
//  Copyright © 2020 Ilia Gutu. All rights reserved.
//

@testable import Luhnatic

final class MockCardDataProvider: CardDataProviderType {

    var getInfoFuncWasCalled = false
    var resultStub: Result<CardInfo, CardDataProviderError>!
    
    func getInfo(_ cardNumber: String, completion: @escaping (Result<CardInfo, CardDataProviderError>) -> Void) {
        getInfoFuncWasCalled = true
        completion(resultStub)
    }
}

