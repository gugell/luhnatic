//
//  MockCardValidator.swift
//  LuhnaticTests
//
//  Created by Ilia Gutu on 2/20/20.
//  Copyright © 2020 Ilia Gutu. All rights reserved.
//

@testable import Luhnatic

final class MockCardValidator: CardValidatorType {
    
    var mockError: Error?
    var validateFuncWasCalled = false
    func validate(_ cardNumber: String) throws {
        validateFuncWasCalled = true
        if let error = mockError {
            throw error
        }
    }
}
