//
//  MockURLSession.swift
//  LuhnaticTests
//
//  Created by Ilia Gutu on 2/20/20.
//  Copyright © 2020 Ilia Gutu. All rights reserved.
//
@testable import Luhnatic

final class MockTask: Task {
    
    var resumeFuncWasCalled = false
    func resume() {
        resumeFuncWasCalled = true
    }

    var cancelFuncWasCalled = false
    func cancel() {
        cancelFuncWasCalled = true
    }
}

final class MockURLSession: URLSessionType {
    
    var dataTaskFuncWasCalled = false
    var dataTaskStub: (Data?, URLResponse?, Error?)!
    
    func dataTask(with request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void) -> Task {
        dataTaskFuncWasCalled = true
        completion(dataTaskStub.0, dataTaskStub.1, dataTaskStub.2)

        return MockTask()
    }
}
